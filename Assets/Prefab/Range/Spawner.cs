using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.MLAgents;
public class Spawner : MonoBehaviour
{
    public float cd;
    public bool isRun;
    public GameObject targetObj;
    public Queue<Transform> targets;
    public Controller player;

    private void Awake()
    {
        targets = new Queue<Transform>();
    }
    private void Start()
    {
        StartCoroutine(Spawn());
    }

    IEnumerator Spawn()
    {
        Collider2D collider = GetComponent<Collider2D>();
        Bounds bounds = collider.bounds;
        while (true)
        {
            Transform target = Instantiate(targetObj, new Vector3(
                Random.Range(bounds.min.x, bounds.max.x), 
                Random.Range(bounds.min.y, bounds.max.y), 0), 
                Quaternion.identity).transform;
            Target targetScript = target.GetComponent<Target>();
            targetScript.spawn = this;
            targets.Enqueue(target);
            if (targets.Count > 10)
            {
                 player.AddReward(-0.05f);
                print(player.GetCumulativeReward());
            }
            if (targets.Count > 30)
            {
                player.EndEpisode();
                Restart();
            }
            yield return new WaitForSeconds(cd);
        }
    }

    public Transform nextTarget()
    {
        try
        {
            while (targets.Peek() == null)
            {
                targets.Dequeue();
            }
        } catch
        {
            return null;
        }
        return targets.Peek();
    }


    public void Restart()
    {
        while (targets.Count != 0 && targets.Peek() != null)
        {
            Destroy(targets.Dequeue().gameObject);
        }
    }
}
