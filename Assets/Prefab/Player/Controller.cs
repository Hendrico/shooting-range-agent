using System.Collections;
using System.Collections.Generic;
using Unity.MLAgents;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Sensors;
using UnityEngine;
public class Controller : Agent
{
    public LayerMask mask;

    public float cd;
    private bool onCd;
    public float speed;
    private float cdTime;
    private Vector3 startPos;
    public Manager manager;
    public Transform target;
    public bool isRun;
    public int miss;
    private int curMiss;
    

    public override void Initialize()
    {
        startPos = transform.position;
        StartCoroutine(TargetFinder());
    }

    public override void CollectObservations(VectorSensor sensor)
    {
        if (target != null)
        {
            sensor.AddObservation(transform.position.x - target.position.x);
        }
        
    }
    
    public override void OnActionReceived(ActionBuffers actions)
    {
        /*
         * 0 idle
         * 1 left
         * 2 right
         */
      
        Move(Mathf.FloorToInt(actions.DiscreteActions[0] == 1 ? -1 : (actions.DiscreteActions[0] == 2 ? 1 : 0)));
        if (!onCd)
        {
            if (Mathf.FloorToInt(actions.DiscreteActions[1]) == 1)
            {
                Shoot();
            }
        }
        else
        {
            Reload();
        }
    }

    public override void OnEpisodeBegin()
    {
        transform.position = startPos;
        cdTime = 0;
        onCd = false;
    }

    public override void Heuristic(in ActionBuffers actionsOut)
    {
        ActionSegment<int> discretesOut = actionsOut.DiscreteActions;
        discretesOut[0] = 0; // Move
        discretesOut[1] = 0; // Shoot
        if (Input.GetKey(KeyCode.A))
        {
            discretesOut[0] = 1;

        }
        else if (Input.GetKey(KeyCode.D))
        {
            discretesOut[0] = 2;
        }
        if (Input.GetKey(KeyCode.Space))
        {
            discretesOut[1] = 1;
        }
    }
    private void FixedUpdate()
    {
        RequestDecision();
    }

    IEnumerator TargetFinder()
    {
        while (isRun)
        {
            if (target == null)
            {
                target = manager.spawn.nextTarget(); 
            }
            yield return new WaitForEndOfFrame();

        }
    }

    private void Move(int direction)
    {
        transform.position += speed * (Vector3)Vector2.right * direction * Time.deltaTime;
    }

    private void Shoot()
    {
        cdTime = cd;
        onCd = true;
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.up, float.MaxValue, mask);
        if (hit.transform != null)
        {
            manager.point += 1;
            AddReward(0.1f);
            print(GetCumulativeReward());
            Destroy(hit.transform.gameObject);
        }

    }

    private void Update()
    {
        RaycastHit2D test = Physics2D.Raycast(transform.position, Vector2.up, float.MaxValue, mask);
        if (test.transform != null)
        {
            Debug.DrawRay(transform.position, Vector2.up * 10f, Color.red);
        }
        else
        {
            Debug.DrawRay(transform.position, Vector2.up * 10f, Color.green);
        }
    }

    private void Reload()
    {
        cdTime -= Time.deltaTime;
        if (cdTime <= 0)
        {
            onCd = false;
            cdTime = 0;
        }
    }
}
